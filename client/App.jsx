/* eslint-disable linebreak-style */
import React from 'react';

export default function App() {
  return (
    <>
      <div className="container-fluid">
        <div className="container">
          <div className="row">
            <div className="col-100">
              <h1>hello world</h1>
              <div className="row">
                <div className="col-50 col-xs-100 col-sm-100">
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. In voluptatum temporibus consectetur, debitis deleniti voluptate sit rem quam
                    impedit libero officiis sunt quidem molestias? Modi dolorem magnam enim iusto deserunt?
                  </p>
                </div>
                <div className="col-50 col-xs-100 col-sm-100">
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam aspernatur magni, sequi sapiente aperiam quam sed suscipit doloremque ex
                    similique. Maxime itaque quibusdam earum repellendus sunt voluptates exercitationem dolores veritatis.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
