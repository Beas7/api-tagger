## Reference

- columns are wrapped inside rows.
- there's no limit about how many columns can be wrapped inside a row.
- define a row using a div (or din interface such header, footer, aside...) `<div class="row">`
- define a column and it's width (relative to it's parent row) `<div class="col-50">`

in the example, it's a column that occupies the 50% of it's parent row.

- you can choose up to 5 breakpoints + default state for any colum:

  - default: `col-50`
  - xs: `col-xs-50`
  - sm: `col-sm-50`
  - md: `col-md-50`
  - lg: `col-lg-50`
  - xl: `col-xl-50`

- you can choose the desired number, from 1 to 100.

- you can define the grid using row modifiers as well, see the example below:

```
<div class="row row-3">
  <div class="col">
```

this will make the cols inside this row to fit in a 100/3 shape, making them 33.333% each.

- define a breakpoint to set rules depending on the viewport as you do with columms: `row row-sm-8 row-md-6`

- you can combine them both as well:

```
  <div class="row row-md-3">
    <div class="col-sm-50  col-lg-25"> Element</div>
```

this way, the columns will take 25% width when large, 33.3333% when medium and 50% when small.
