const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

function courseExists(id) {
  const course = courses.find((c) => c.id === parseInt(id));
  return course;
}

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Listening on port ${port}`));
